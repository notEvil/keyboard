import keyboard._ecodes as k__ecodes
import lxml.etree as l_etree
import lxml.html as l_html
import requests
import argparse
import pickle
import urllib.parse as u_parse
from pprint import pprint


URL = "http://kbdlayout.info"


parser = argparse.ArgumentParser()

parser.add_argument("--from", required=True)
parser.add_argument("--to", required=True)

arguments = parser.parse_args()


if True:
    document_node = l_html.document_fromstring(requests.get(URL).text)

    def get_xml_string(name, document_node):
        if name == "Colemak":
            name = "US"

        _ = document_node.xpath(".//a[normalize-space(text())='{}']".format(name))[0]
        return requests.get(u_parse.urljoin(URL, _.get("href") + "download/xml")).text

    from_xml_string = get_xml_string(getattr(arguments, "from"), document_node)
    to_xml_string = get_xml_string(getattr(arguments, "to"), document_node)

    if False:
        with open("./xmls", "wb") as file:
            pickle.dump((from_xml_string, to_xml_string), file)

else:
    with open("./xmls", "rb") as file:
        from_xml_string, to_xml_string = pickle.load(file)


def get_keys(xml_string, name):
    keys = dict(_get_keys(l_etree.fromstring(xml_string.encode("latin-1").decode())))

    if name == "Colemak":
        _ = zip("abcdefghijklmnpqrstuvwxyz", "abcgkethlynumjrqsdfivwxoz")
        vk_names = {a.upper(): b.upper() for a, b in _}
        vk_names["O"] = "OEM_1"
        vk_names["OEM_1"] = "P"

        _ = keys.items()
        scan_codes = {
            vk_name: scan_code for scan_code, (vk_name, _) in _ if vk_name in vk_names
        }
        _ = scan_codes.items()
        keys.update(
            {scan_codes[vk_names[vk_name]]: keys[scan_code] for vk_name, scan_code in _}
        )

    return keys


def _get_keys(xml):
    (kl_node,) = xml.xpath("/KeyboardLayout")
    ralt_is_altgr = dict(true=True, false=False)[kl_node.get("RightAltIsAltGr")]

    for pk_node in kl_node.xpath("./PhysicalKeys/PK"):
        scan_code = int(pk_node.get("SC"), 16)
        vk_name = _vk_name(pk_node.get("VK"))

        results = {}
        for result_node in pk_node.xpath("./Result"):
            result_string = result_node.get("Text")
            if result_string is None:
                dk_table_nodes = result_node.xpath("./DeadKeyTable")
                if len(dk_table_nodes) == 0:
                    continue

                modifier_names = _get_modifiers(result_node, ralt_is_altgr)

                (dk_table_node,) = dk_table_nodes
                for result_node in dk_table_node.xpath("./Result"):
                    result_string = result_node.get("Text")
                    results[result_string] = modifier_names | {result_node.get("With")}

                continue

            modifier_names = _get_modifiers(result_node, ralt_is_altgr)
            _existing = results.get(result_string)
            if _existing is not None and len(_existing) <= len(modifier_names):
                continue
            results[result_string] = modifier_names

        yield (scan_code, (vk_name, results))


def _get_modifiers(node, ralt_is_altgr):
    modifier_names = {_vk_name(string) for string in node.get("With", "").split()}

    if ralt_is_altgr:
        _ = {"CONTROL", "MENU"}
        if modifier_names.issuperset(_):
            modifier_names -= _
            modifier_names.add("RMENU")

    return modifier_names


def _vk_name(string):
    assert string.startswith("VK_")
    return string[len("VK_") :]


from_keys = get_keys(from_xml_string, getattr(arguments, "from"))
to_keys = get_keys(to_xml_string, arguments.to)


if False:
    print()
    print("# @from  virtual key: virtual key  @to")

    vk_names = {}

    for scan_code in from_keys.keys() | to_keys.keys():
        from_key = from_keys.get(scan_code)
        to_key = to_keys.get(scan_code)

        if from_key is not None and to_key is not None:
            from_vk, _ = from_key
            to_vk, _ = to_key

            if from_vk != to_vk:
                vk_names[from_vk] = to_vk

    pprint(vk_names)


class _Variable:
    def __init__(self, name):
        super().__init__()

        self.name = name

    def __repr__(self):
        return self.name

    def __lt__(self, object):
        if type(object) is not _Variable:
            raise NotImplemented

        return self.name < object.name


_ = _Variable("_shift")
_MODIFIER_SETS = {(): _Variable("_none"), ("SHIFT",): _, ("RMENU",): _Variable("_ralt")}


def _get_modifier_set(modifier_names):
    _ = tuple(sorted(modifier_names))
    return _MODIFIER_SETS.get(_, _)


def _get_key_name(scan_code):
    key = k__ecodes._WIN32_SC_KEYS.get((scan_code & 0xFF, 0xFF < scan_code))
    if key is None:
        return str(scan_code)

    return "e_ecodes.{}".format(k__ecodes.keys[key])


print()
print("# @to  (key, modifiers): str")

result_strings = {
    (scan_code, frozenset(modifier_names)): result_string
    for scan_code, (_, results) in to_keys.items()
    for result_string, modifier_names in results.items()
}

_ = {
    (
        _Variable(_get_key_name(scan_code)),
        _get_modifier_set(modifier_names),
    ): result_string
    for (scan_code, modifier_names), result_string in result_strings.items()
}
pprint(_)


print()
print("# @to  (key, modifiers): (key, modifiers)  @from  [1/2]")

_simple_events = {}

for scan_code, (_, results) in from_keys.items():
    for result_string, modifier_names in results.items():
        _simple_events.setdefault(result_string, []).append((scan_code, modifier_names))


simple_events = {}

for to_scan_code, (_, results) in to_keys.items():
    for result_string, to_modifiers in results.items():
        to_simple_event = (to_scan_code, frozenset(to_modifiers))

        _ = _simple_events.get(result_string)
        if _ is None:
            simple_events[to_simple_event] = (None, result_string)
            continue

        from_simple_events = [
            (from_scan_code, from_modifiers) for from_scan_code, from_modifiers in _
        ]
        if to_simple_event in from_simple_events:
            continue
        from_simple_event = min(from_simple_events, key=lambda _: len(_[1]))
        simple_events[to_simple_event] = (from_simple_event, result_string)

scan_codes = {}

for (to_scan_code, to_modifiers), (
    (from_scan_code, from_modifiers),
    _,
) in simple_events.items():
    _ = result_strings[(to_scan_code, to_modifiers)]
    if len(_) == 1 and "a" <= _ and _ <= "z":
        scan_codes[to_scan_code] = from_scan_code


print("{")

for (to_scan_code, to_modifiers), (
    (from_scan_code, from_modifiers),
    result_string,
) in sorted(simple_events.items()):
    if (
        from_scan_code == scan_codes.get(to_scan_code, to_scan_code)
        and from_modifiers == to_modifiers
    ):
        continue

    print(
        " " * 4,
        (_Variable(_get_key_name(to_scan_code)), _get_modifier_set(to_modifiers)),
        ": ",
        (_Variable(_get_key_name(from_scan_code)), _get_modifier_set(from_modifiers)),
        ",  # ",
        result_string,
        sep="",
    )

print("}")

print()
print("# @to  key: key  @from  [2/2]")

_ = {
    _Variable(_get_key_name(to_scan_code)): _Variable(_get_key_name(from_scan_code))
    for to_scan_code, from_scan_code in scan_codes.items()
}
pprint(_)

import evdev.ecodes as e_ecodes
from pprint import pprint


_ = {name: object for name, object in vars(e_ecodes).items() if name.startswith("KEY_")}
for name, object in sorted(_.items()):
    print(name, "=", object)

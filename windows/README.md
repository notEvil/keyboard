# Windows support

I usually don't support Windows for various reasons. This Python package is an exception because I got so used to it that I feel uncomfortable without it. So you can expect the same level of support for Windows as for Linux.

## Quickstart

- use [*zero*](https://gitlab.com/notEvil/zero) and its `keyboard/prepare.bat`
- find `run_example.py` within *zero* and execute it with Python
    - if you want to try [*Master Password*](https://gitlab.com/MasterPassword/MasterPassword), add `--mpw`
    - then, if you want a small docker container, execute `mpw/build.py` with Python
- if you intend to use your own script, use *zero*s `keyboard/execute.py` directly

## evdev for Windows

`keyboard.windows.evdev` is a Python package which provides a thin layer between the application and [*pynput*](https://github.com/moses-palmer/pynput). The main contribution of this package is `keyboard.windows.evdev.ecodes`, the rest is merely leveraging the powers of *pynput*.

Currently, it requires a patched version of *pynput* to work. I will try to get the feature into *pynput* and till then adjust the patches whenever there is a new release.

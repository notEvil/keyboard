import pathlib
import subprocess
import sys


PATH = pathlib.Path(__file__).parent


_ = (
    [sys.executable, str(pathlib.Path(PATH.parent.parent, "execute.py"))]
    + sys.argv[1:]
    + [str(pathlib.Path(PATH, "example.py"))]
)
assert subprocess.Popen(_).wait() == 0

import os
import pathlib
import subprocess
import sys


PATH = pathlib.Path(__file__).parent


_ = str(pathlib.Path(os.environ["ZERO_PATH"], "pipenv", "prepare_environment.py"))
assert subprocess.Popen([sys.executable, _], cwd=PATH).wait() == 0

_ = [
    sys.executable,
    "-m",
    "pipenv",
    "run",
    "python",
    str(pathlib.Path(PATH, "__prepare.py")),
]
assert subprocess.Popen(_, cwd=PATH).wait() == 0

import requests
import subprocess
import pathlib
import sys
import zipfile


PATH = pathlib.Path(__file__).parent


def download(url_string, path):
    _ = requests.get(url_string, stream=True).iter_content(chunk_size=4096)
    with open(path, "wb") as file:
        for bytes in _:
            file.write(bytes)


zip_path = pathlib.Path(PATH, "MasterPassword-master.zip")

if not zip_path.exists():
    print("> downloading Master Password")
    download(
        "https://gitlab.com/MasterPassword/MasterPassword/-/archive/master/MasterPassword-master.zip",
        zip_path,
    )


mpw_path = pathlib.Path(PATH, "MasterPassword-master")

if not mpw_path.exists():
    print("> extracting Master Password")
    with zipfile.ZipFile(zip_path) as zip_file:
        zip_file.extractall(PATH)


print("> patching Master Password")
_ = str(pathlib.Path(PATH, "_patch"))
_ = subprocess.Popen([sys.executable, "-m", "patch", "-d", str(mpw_path), _]).wait()
assert _ == 0


print("> creating build container")
_ = ["podman", "build", str(pathlib.Path(PATH, "build_env"))]
process = subprocess.Popen(_, stdout=subprocess.PIPE)

for bytes in process.stdout:
    _ = sys.stdout.buffer
    _.write(bytes)
    _.flush()

assert process.wait() == 0
container_id = bytes.decode().strip()


print("> building Master Password CLI")
_ = [
    "podman",
    "run",
    "--rm",
    "-v",
    "{}:/build".format(pathlib.Path(mpw_path, "platform-independent", "c")),
    "-w",
    "/build/cli",
    container_id,
    "sh",
    "-c",
    "cmake -DBUILD_MPW_TESTS=ON . && make && ./mpw-tests",
]
assert subprocess.Popen(_).wait() == 0

print("> creating Master Password CLI container")
assert subprocess.Popen(["podman", "build", str(PATH)]).wait() == 0

print("> use this id ;)")
print("> Done")
input("Press ENTER to exit ")

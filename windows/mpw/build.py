import pathlib
import subprocess
import sys


PATH = pathlib.Path(__file__).parent

_ = str(pathlib.Path(PATH, "_build.py"))
_ = [sys.executable, "-m", "pipenv", "run", "python", _]
assert subprocess.Popen(_, cwd=PATH).wait() == 0

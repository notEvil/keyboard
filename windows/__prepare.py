import pynput
import pathlib
import subprocess
import sys


PATH = pathlib.Path(__file__).parent


print("> patching Pynput")

pynput_path = pathlib.Path(pynput.__file__).parent

for name in ["win32.py.patch", "_win32.py.patch"]:
    _ = [
        sys.executable,
        "-m",
        "patch",
        "-d",
        str(pynput_path),
        str(pathlib.Path(PATH, "pynput", name)),
    ]
    assert subprocess.Popen(_).wait() == 0

"""
CAVEATS
- this is just an example
  - you may use it for initial tests
  - you may use the general structure as baseline
  - I really don't recommend any specific configuration
"""


import keyboard
import keyboard.mpw as k_mpw  # optional
import evdev.ecodes as e_ecodes
import argparse
import pathlib


PATH = pathlib.Path(__file__).parent  # path to this script


_none = frozenset()
_shift = frozenset({keyboard.SHIFT})
# generated with `python kbdlayout.info.py --from US --to Colemak`
_ = {
    (e_ecodes.KEY_0, _none): "0",
    (e_ecodes.KEY_0, _shift): ")",
    (e_ecodes.KEY_1, _none): "1",
    (e_ecodes.KEY_1, _shift): "!",
    (e_ecodes.KEY_102ND, _none): "\\",
    (e_ecodes.KEY_102ND, _shift): "|",
    (e_ecodes.KEY_2, _none): "2",
    (e_ecodes.KEY_2, _shift): "@",
    (e_ecodes.KEY_3, _none): "3",
    (e_ecodes.KEY_3, _shift): "#",
    (e_ecodes.KEY_4, _none): "4",
    (e_ecodes.KEY_4, _shift): "$",
    (e_ecodes.KEY_5, _none): "5",
    (e_ecodes.KEY_5, _shift): "%",
    (e_ecodes.KEY_6, _none): "6",
    (e_ecodes.KEY_6, _shift): "^",
    (e_ecodes.KEY_7, _none): "7",
    (e_ecodes.KEY_7, _shift): "&",
    (e_ecodes.KEY_8, _none): "8",
    (e_ecodes.KEY_8, _shift): "*",
    (e_ecodes.KEY_9, _none): "9",
    (e_ecodes.KEY_9, _shift): "(",
    (e_ecodes.KEY_A, _none): "a",
    (e_ecodes.KEY_A, _shift): "A",
    (e_ecodes.KEY_APOSTROPHE, _none): "'",
    (e_ecodes.KEY_APOSTROPHE, _shift): '"',
    (e_ecodes.KEY_B, _none): "b",
    (e_ecodes.KEY_B, _shift): "B",
    (e_ecodes.KEY_BACKSLASH, _none): "\\",
    (e_ecodes.KEY_BACKSLASH, _shift): "|",
    (e_ecodes.KEY_C, _none): "c",
    (e_ecodes.KEY_C, _shift): "C",
    (e_ecodes.KEY_COMMA, _none): ",",
    (e_ecodes.KEY_COMMA, _shift): "<",
    (e_ecodes.KEY_D, _none): "s",
    (e_ecodes.KEY_D, _shift): "S",
    (e_ecodes.KEY_DOT, _none): ".",
    (e_ecodes.KEY_DOT, _shift): ">",
    (e_ecodes.KEY_E, _none): "f",
    (e_ecodes.KEY_E, _shift): "F",
    (e_ecodes.KEY_EQUAL, _none): "=",
    (e_ecodes.KEY_EQUAL, _shift): "+",
    (e_ecodes.KEY_F, _none): "t",
    (e_ecodes.KEY_F, _shift): "T",
    (e_ecodes.KEY_G, _none): "d",
    (e_ecodes.KEY_G, _shift): "D",
    (e_ecodes.KEY_GRAVE, _none): "`",
    (e_ecodes.KEY_GRAVE, _shift): "~",
    (e_ecodes.KEY_H, _none): "h",
    (e_ecodes.KEY_H, _shift): "H",
    (e_ecodes.KEY_I, _none): "u",
    (e_ecodes.KEY_I, _shift): "U",
    (e_ecodes.KEY_J, _none): "n",
    (e_ecodes.KEY_J, _shift): "N",
    (e_ecodes.KEY_K, _none): "e",
    (e_ecodes.KEY_K, _shift): "E",
    (e_ecodes.KEY_KP0, _none): "0",
    (e_ecodes.KEY_KP1, _none): "1",
    (e_ecodes.KEY_KP2, _none): "2",
    (e_ecodes.KEY_KP3, _none): "3",
    (e_ecodes.KEY_KP4, _none): "4",
    (e_ecodes.KEY_KP5, _none): "5",
    (e_ecodes.KEY_KP6, _none): "6",
    (e_ecodes.KEY_KP7, _none): "7",
    (e_ecodes.KEY_KP8, _none): "8",
    (e_ecodes.KEY_KP9, _none): "9",
    (e_ecodes.KEY_KPASTERISK, _none): "*",
    (e_ecodes.KEY_KPDOT, _none): ".",
    (e_ecodes.KEY_KPMINUS, _none): "-",
    (e_ecodes.KEY_KPPLUS, _none): "+",
    (e_ecodes.KEY_L, _none): "i",
    (e_ecodes.KEY_L, _shift): "I",
    (e_ecodes.KEY_LEFTBRACE, _none): "[",
    (e_ecodes.KEY_LEFTBRACE, _shift): "{",
    (e_ecodes.KEY_M, _none): "m",
    (e_ecodes.KEY_M, _shift): "M",
    (e_ecodes.KEY_MINUS, _none): "-",
    (e_ecodes.KEY_MINUS, _shift): "_",
    (e_ecodes.KEY_N, _none): "k",
    (e_ecodes.KEY_N, _shift): "K",
    (e_ecodes.KEY_O, _none): "y",
    (e_ecodes.KEY_O, _shift): "Y",
    (e_ecodes.KEY_P, _none): ";",
    (e_ecodes.KEY_P, _shift): ":",
    (e_ecodes.KEY_Q, _none): "q",
    (e_ecodes.KEY_Q, _shift): "Q",
    (e_ecodes.KEY_R, _none): "p",
    (e_ecodes.KEY_R, _shift): "P",
    (e_ecodes.KEY_RIGHTBRACE, _none): "]",
    (e_ecodes.KEY_RIGHTBRACE, _shift): "}",
    (e_ecodes.KEY_S, _none): "r",
    (e_ecodes.KEY_S, _shift): "R",
    (e_ecodes.KEY_SEMICOLON, _none): "o",
    (e_ecodes.KEY_SEMICOLON, _shift): "O",
    (e_ecodes.KEY_SLASH, _none): "/",
    (e_ecodes.KEY_SLASH, _shift): "?",
    (e_ecodes.KEY_SPACE, _none): " ",
    (e_ecodes.KEY_T, _none): "g",
    (e_ecodes.KEY_T, _shift): "G",
    (e_ecodes.KEY_U, _none): "l",
    (e_ecodes.KEY_U, _shift): "L",
    (e_ecodes.KEY_V, _none): "v",
    (e_ecodes.KEY_V, _shift): "V",
    (e_ecodes.KEY_W, _none): "w",
    (e_ecodes.KEY_W, _shift): "W",
    (e_ecodes.KEY_X, _none): "x",
    (e_ecodes.KEY_X, _shift): "X",
    (e_ecodes.KEY_Y, _none): "j",
    (e_ecodes.KEY_Y, _shift): "J",
    (e_ecodes.KEY_Z, _none): "z",
    (e_ecodes.KEY_Z, _shift): "Z",
}
characters = {
    keyboard.SimpleEvent(key_code, modifiers, None): character
    for (key_code, modifiers), character in _.items()
}


"""
- the following allows you to map a layout onto another
  - e.g. map Colemak onto US because there is no builtin Colemak
  - might save you some OS-level layout switches (Windows+Space)
"""

# generated with `python kbdlayout.info.py --from US --to Colemak`
_ = {
    (e_ecodes.KEY_P, _none): (e_ecodes.KEY_SEMICOLON, _none),  # ;
    (e_ecodes.KEY_P, _shift): (e_ecodes.KEY_SEMICOLON, _shift),  # :
}  # simple because Colemak is very close to US; there is a lot more for `--from German`
us_simple_events = {
    keyboard.SimpleEvent(from_key_code, from_modifiers, None): keyboard.SimpleEvent(
        to_key_code, to_modifiers, None
    )
    for (from_key_code, from_modifiers), (to_key_code, to_modifiers) in _.items()
}

# generated with `python kbdlayout.info.py --from US --to Colemak`
us_key_codes = {
    e_ecodes.KEY_D: e_ecodes.KEY_S,
    e_ecodes.KEY_E: e_ecodes.KEY_F,
    e_ecodes.KEY_F: e_ecodes.KEY_T,
    e_ecodes.KEY_G: e_ecodes.KEY_D,
    e_ecodes.KEY_I: e_ecodes.KEY_U,
    e_ecodes.KEY_J: e_ecodes.KEY_N,
    e_ecodes.KEY_K: e_ecodes.KEY_E,
    e_ecodes.KEY_L: e_ecodes.KEY_I,
    e_ecodes.KEY_N: e_ecodes.KEY_K,
    e_ecodes.KEY_O: e_ecodes.KEY_Y,
    e_ecodes.KEY_R: e_ecodes.KEY_P,
    e_ecodes.KEY_S: e_ecodes.KEY_R,
    e_ecodes.KEY_SEMICOLON: e_ecodes.KEY_O,
    e_ecodes.KEY_T: e_ecodes.KEY_G,
    e_ecodes.KEY_U: e_ecodes.KEY_L,
    e_ecodes.KEY_Y: e_ecodes.KEY_J,
}


"""
- keys in evdev.ecodes correspond to physical keys (scan code) on the US layout
  - e.g. KEY_GRAVE is the key left of KEY_1  and  KEY_SEMICOLON is the key below KEY_P
"""

transformations = [
    keyboard.CategorizeEvents(
        "in"
    ),  # print result of evdev.categorize prefixed with "in: "
    keyboard.Pausable(
        e_ecodes.KEY_RIGHTCTRL, ungrab=False  # ungrab must be False on Windows
    ),  # suspend/resume with right ctrl
    keyboard.RetimeRepeats(),  # adjust key repeat delays
    keyboard.SubstituteKeyCodes(
        {
            e_ecodes.KEY_BACKSPACE: None,  # disable key
            # swap left alt with left ctrl
            e_ecodes.KEY_LEFTCTRL: e_ecodes.KEY_LEFTALT,
            e_ecodes.KEY_LEFTALT: e_ecodes.KEY_LEFTCTRL,
        }
    ),
    keyboard.ModifierAsKey(
        {e_ecodes.KEY_RIGHTSHIFT: e_ecodes.KEY_SPACE}
    ),  # use right shift as space
    keyboard.ToSimpleEvents(),
    keyboard.SubstituteSimpleEvents(
        {
            keyboard.SimpleEvent(
                e_ecodes.KEY_A, {keyboard.CTRL}, None
            ): keyboard.SimpleEvent(
                e_ecodes.KEY_BACKSPACE, set(), None
            )  # map ctrl+a to backspace; mind previous transformations ;)
        }
    ),
    k_mpw.WithMasterPassword(
        "John Doe",
        [
            pathlib.Path(PATH.parent, "example_password")
        ],  # file was generated with `python -m keyboard.mpw > example_password` and "password" (without quotes)
        characters,
        version_path=pathlib.Path(PATH.parent, "example_versions"),  # optional
        mpw_parts=["podman", "run", "-i", "--rm", "docker://miripii/mpw-cli"],
    ),
    keyboard.PrintSimpleEvents(),
    keyboard.SubstituteSimpleEvents(us_simple_events, key_code_dictionary=us_key_codes),
    keyboard.ToEvents(),  # inverse of keyboard.ToSimpleEvents
    keyboard.DelayEvents(),  # optional, fixes an unknown bug on one of my setups
    keyboard.CategorizeEvents("out"),
]
keyboard.main(None, transformations)

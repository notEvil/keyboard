import shutil
import subprocess


if shutil.which("podman") is None:
    print("> installing Podman")
    assert subprocess.Popen(["winget", "install", "podman"]).wait() == 0

process = subprocess.Popen(["podman", "machine", "list"], stdout=subprocess.PIPE)

line_count = 0
running = False
for line_bytes in process.stdout:
    line_string = line_bytes.decode().strip()
    if line_string != "":
        line_count += 1

    if "Currently running" in line_string:
        running = True
        break

assert process.wait() == 0

if line_count == 1:
    print("> creating Podman machine")
    assert subprocess.Popen(["podman", "machine", "init"]).wait() == 0

if not running:
    print("> starting Podman machine")
    assert subprocess.Popen(["podman", "machine", "start"]).wait() == 0

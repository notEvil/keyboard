import keyboard
import evdev.ecodes as e_ecodes
import collections
import hashlib
import os
import pickle
import re
import subprocess


def get_password_info(password):
    hash_salt = os.urandom(32)
    hash = _get_password_hash(password, hash_salt)
    return pickle.dumps((len(password), hash_salt, hash))


class WithMasterPassword(keyboard.Transformation):
    """
    - substitutes {master password}@[{site}[#{version}]](<TAB>|<ENTER>) by the password computed by mpw
    - uses "sudo" if site is not defined
    """

    def __init__(
        self,
        name,
        password_info_paths,
        character_by_simple_event,
        version=1,
        backspace_key_codes=(e_ecodes.KEY_BACKSPACE,),
        version_path=None,
        mpw_parts=("mpw",),
    ):
        super().__init__()

        self.name = name
        self.password_info_paths = password_info_paths
        self.character_by_simple_event = character_by_simple_event
        self.version = version
        self.backspace_key_codes = backspace_key_codes
        self.version_path = version_path
        self.mpw_parts = mpw_parts

        if self.version_path is None or not self.version_path.exists():
            self._versions = {}

        else:
            with open(self.version_path, "rb") as file:
                self._versions = pickle.load(file)

    def get_transformed(self, simple_event_stream, u_input, input_device):
        password_infos = []

        for password_info_path in self.password_info_paths:
            with open(password_info_path, "rb") as file:
                password_info = pickle.load(file)
                password_infos.append(password_info)

        character_by_key = {
            simple_event.get_key(): character
            for simple_event, character in self.character_by_simple_event.items()
        }
        simple_event_by_character = {
            character: simple_event
            for simple_event, character in self.character_by_simple_event.items()
        }

        backspace_keys = {
            keyboard.SimpleEvent(key_code, frozenset(), None).get_key()
            for key_code in self.backspace_key_codes
        }
        trigger_key = simple_event_by_character["@"].get_key()
        finalize_keys = {
            keyboard.SimpleEvent(key_code, frozenset(), None).get_key()
            for key_code in [e_ecodes.KEY_ENTER, e_ecodes.KEY_TAB]
        }

        _ = max(max(password_length for password_length, _, _ in password_infos), 64)
        characters = collections.deque([], _)
        password = None

        for simple_event in simple_event_stream:
            if simple_event.key_code is None:
                yield simple_event
                continue

            key = simple_event.get_key()

            if key in backspace_keys:
                if len(characters) != 0:
                    characters.pop()

                yield simple_event
                continue

            if key == trigger_key and password is None:
                for password_length, hash_salt, password_hash in password_infos:
                    if len(characters) < password_length:
                        continue

                    password = "".join(list(characters)[-password_length:])

                    if _get_password_hash(password, hash_salt) != password_hash:
                        continue

                    characters.clear()
                    _ = e_ecodes.KEY_BACKSPACE
                    yield from [
                        keyboard.SimpleEvent(_, frozenset(), simple_event.time)
                    ] * password_length
                    break

                else:
                    password = None

                if password is not None:
                    continue

            if password is not None and key in finalize_keys:
                _ = e_ecodes.KEY_BACKSPACE
                yield from [
                    keyboard.SimpleEvent(_, frozenset(), simple_event.time)
                ] * len(characters)

                site_string = "".join(characters)

                match = re.search(r"#\d+$", site_string)
                if match is None:
                    _ = self._versions.get(password_hash, {})
                    version_string = str(_.get(site_string, self.version))

                else:
                    version_string = match.group(0)[1:]
                    site_string = site_string[: -(len(version_string) + 1)]
                    _ = int(version_string)
                    self._update_versions(_, site_string, password_hash)

                if len(site_string) == 0:
                    site_string = "sudo"

                password = _get_password(
                    site_string, version_string, password, self.name, self.mpw_parts
                )

                for character in password:
                    _ = simple_event_by_character[character]
                    yield _.copy(time=simple_event.time)

                characters.clear()
                password = None
                yield simple_event
                continue

            character = character_by_key.get(key)
            if character is None:
                characters.clear()
                password = None
                yield simple_event
                continue

            characters.append(character)
            yield simple_event

    def _update_versions(self, version, site_string, password_hash):
        self._versions.setdefault(password_hash, {})[site_string] = version

        if self.version_path is not None:
            with open(self.version_path, "wb") as file:
                pickle.dump(self._versions, file)


def _get_password_hash(password, hash_salt):
    return hashlib.pbkdf2_hmac("sha256", password.encode(), hash_salt, int(1e5))


def _get_password(site_string, version_string, password, name, mpw_parts):
    _ = ["-u", name, "-m", "0", "-c", version_string, "-F", "n", site_string]
    process = subprocess.Popen(
        list(mpw_parts) + _,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
    )
    stdout, _ = process.communicate(input=password.encode())
    return stdout.decode().strip()

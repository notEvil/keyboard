import keyboard.mpw as k_mpw
import getpass
import sys


password = getpass.getpass()
sys.stdout.buffer.write(k_mpw.get_password_info(password))

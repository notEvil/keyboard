import evdev
import evdev.ecodes as e_ecodes
import queue
import threading
import time


class _None:
    pass


OrderedDict = dict


KEY_UP = evdev.KeyEvent.key_up
KEY_DOWN = evdev.KeyEvent.key_down
KEY_HOLD = evdev.KeyEvent.key_hold


class Modifier:
    def __init__(self, name):
        super().__init__()

        self.name = name

    def __repr__(self):
        return "Modifier({})".format(repr(self.name))


SHIFT = Modifier("Shift")
CTRL = Modifier("Ctrl")
LEFT_ALT = Modifier("Left Alt")
RIGHT_ALT = Modifier("Right Alt")
LEFT_META = Modifier("Left Meta")


MODIFIER_BY_KEY_CODE = {
    e_ecodes.KEY_LEFTSHIFT: SHIFT,
    e_ecodes.KEY_RIGHTSHIFT: SHIFT,
    e_ecodes.KEY_LEFTCTRL: CTRL,
    e_ecodes.KEY_RIGHTCTRL: CTRL,
    e_ecodes.KEY_LEFTALT: LEFT_ALT,
    e_ecodes.KEY_RIGHTALT: RIGHT_ALT,
    e_ecodes.KEY_LEFTMETA: LEFT_META,
}

_ = MODIFIER_BY_KEY_CODE.items()
KEY_CODE_BY_MODIFIER = {modifier: key_code for key_code, modifier in _}


def main(device_path, transformations):
    input_device = evdev.InputDevice(device_path)
    u_input = evdev.UInput.from_device(device_path)

    input_device.grab()

    try:
        event_stream = input_device.read_loop()

        event_stream = _pass_non_key_events(event_stream, u_input)
        event_stream = _pass_initial_key_up_events(event_stream, u_input)

        for transformation in transformations:
            event_stream = transformation.get_transformed(
                event_stream, u_input, input_device
            )

        for event in event_stream:
            u_input.write_event(event)
            u_input.syn()

    finally:
        try:
            input_device.ungrab()

        except IOError as exception:
            if exception.args != ("Invalid argument",):
                raise exception


def _pass_non_key_events(event_stream, u_input):
    for event in event_stream:
        if event.type != e_ecodes.EV_KEY:
            u_input.write_event(event)
            continue

        yield event


def _pass_initial_key_up_events(event_stream, u_input):
    start_time = time.monotonic()
    key_codes = []

    for event in event_stream:
        if event.value == KEY_DOWN:
            key_codes.append(event.code)

        else:
            if event.code not in key_codes:
                u_input.write_event(event)
                u_input.syn()
                continue

            if event.value == KEY_UP:
                key_codes.remove(event.code)

        yield event

        if 5 < (time.monotonic() - start_time):
            break

    yield from event_stream


# transformations


class Transformation:
    def get_transformed(self, event_stream, u_input, input_device):
        raise NotImplementedError


class Pausable(Transformation):
    def __init__(self, key_code, ungrab=True):
        super().__init__()

        self.key_code = key_code
        self.ungrab = ungrab

    def get_transformed(self, event_stream, u_input, input_device):
        paused = False

        for event in event_stream:
            if event.code == self.key_code:
                if event.value == KEY_UP:
                    paused = not paused

                    if self.ungrab:
                        if paused:
                            input_device.ungrab()

                        else:
                            input_device.grab()

                continue

            if paused:
                if not self.ungrab:
                    u_input.write_event(event)

                continue

            yield event


class CategorizeEvents(Transformation):
    """
    - prints information about key events provided by evdev.categorize
    """

    def __init__(self, name=None):
        super().__init__()

        self.name = name

    def get_transformed(self, event_stream, u_input, input_device):
        for event in event_stream:
            if self.name is not None:
                print(self.name, ": ", sep="", end="")
            print(evdev.categorize(event))
            yield event


class ToSimpleEvents(Transformation):
    """
    - transforms stream of raw events to stream of simple events
    - see SimpleEvent
    """

    def get_transformed(self, event_stream, u_input, input_device):
        modifier_counts = {}

        for event in event_stream:
            modifier = MODIFIER_BY_KEY_CODE.get(event.code)
            if modifier is not None:
                held = event.value == KEY_HOLD

                if not held:
                    modifier_count = modifier_counts.get(modifier, 0)

                    if event.value == KEY_DOWN:
                        modifier_counts[modifier] = modifier_count + 1

                    else:
                        if modifier_count == 1:
                            del modifier_counts[modifier]

                        else:
                            modifier_counts[modifier] = modifier_count - 1

                _ = frozenset(modifier_counts.keys())
                yield SimpleEvent(
                    None, _, (event.sec, event.usec), held=held
                )  # keep modifiers transparent
                continue

            if event.value == KEY_UP:
                continue

            _ = frozenset(modifier_counts.keys())
            yield SimpleEvent(
                event.code, _, (event.sec, event.usec), held=event.value == KEY_HOLD
            )


class SimpleEvent:
    """
    - unlike raw events, simple events are independent from previous simple events
    """

    def __init__(self, key_code, modifier_set, time, held=None):
        self.key_code = key_code
        self.modifier_set = modifier_set
        self.time = time
        self.held = held

        self._key = None

    def get_key(self):
        return (self.key_code, self.modifier_set)

    def copy(self, key_code=None, time=None, held=None):
        return SimpleEvent(
            self.key_code if key_code is None else key_code,
            self.modifier_set,
            self.time if time is None else time,
            held=self.held if held is None else held,
        )

    def __repr__(self):
        _ = repr(set(self.modifier_set))
        return "SimpleEvent({}, {}, {}, held={})".format(
            repr(self.key_code), _, repr(self.time), repr(self.held)
        )


class PrintSimpleEvents(Transformation):
    def __init__(self, name=None):
        super().__init__()

        self.name = name

    def get_transformed(self, simple_event_stream, u_input, input_device):
        for simple_event in simple_event_stream:
            if self.name is not None:
                print(self.name, ": ", sep="", end="")
            print(simple_event)
            yield simple_event


class ToEvents(Transformation):
    """
    - transforms stream of simple events to stream of raw events
    """

    def get_transformed(self, simple_event_stream, u_input, input_device):
        modifier_set = set()

        for simple_event in simple_event_stream:
            if simple_event.modifier_set != modifier_set:  # update modifiers
                _ = modifier_set - simple_event.modifier_set
                for modifier in _:  # release modifier keys
                    _ = KEY_CODE_BY_MODIFIER[modifier]
                    yield _get_event(_, KEY_UP, time_=simple_event.time)

                _ = simple_event.modifier_set - modifier_set
                for modifier in _:  # press modifier keys
                    _ = KEY_CODE_BY_MODIFIER[modifier]
                    yield _get_event(_, KEY_DOWN, time_=simple_event.time)

                modifier_set = simple_event.modifier_set

            if simple_event.key_code is not None:  # press and release key
                # press key
                _ = simple_event.key_code
                yield _get_event(_, KEY_DOWN, time_=simple_event.time)

                # release key
                yield _get_event(simple_event.key_code, KEY_UP, time_=simple_event.time)


class SubstituteKeyCodes(Transformation):
    """
    - substitutes key codes in raw events
    """

    def __init__(self, key_code_dictionary):
        super().__init__()

        self.key_code_dictionary = key_code_dictionary

    def get_transformed(self, event_stream, u_input, input_device):
        for event in event_stream:
            key_code = self.key_code_dictionary.get(event.code, _None)
            if key_code is _None:
                yield event
                continue

            if key_code is None:
                continue

            yield _get_event(key_code, event.value, time_=(event.sec, event.usec))


class ModifierAsKey(Transformation):
    """
    - injects key events if modifiers are not used as modifiers
    - with timeout
    """

    def __init__(self, key_code_dictionary, timeout=0.25):
        super().__init__()

        self.key_code_dictionary = key_code_dictionary
        self.timeout = timeout

    def get_transformed(self, event_stream, u_input, input_device):
        begin_key_code = None
        begin_time = None
        any_other_event = False

        for event in event_stream:
            key_code = self.key_code_dictionary.get(event.code)
            if key_code is not None:
                yield event

                if event.value == KEY_DOWN:  # begin
                    begin_key_code = key_code
                    begin_time = _get_time(event)
                    any_other_event = False
                    continue

                if (
                    key_code == begin_key_code
                    and event.value == KEY_UP
                    and not any_other_event
                    and (_get_time(event) - begin_time) <= self.timeout
                ):  # end
                    yield _get_event(key_code, KEY_DOWN, time_=(event.sec, event.usec))
                    yield _get_event(key_code, KEY_UP, time_=(event.sec, event.usec))

                continue

            yield event
            if event.value != KEY_UP:
                any_other_event = True


def _get_time(event):
    return event.sec + event.usec * 1e-6


class SubstituteSimpleEvents(Transformation):
    """
    - substitutes simple events by other simple events
    """

    def __init__(self, simple_event_dictionary, key_code_dictionary=None):
        super().__init__()

        self.simple_event_dictionary = simple_event_dictionary
        self.key_code_dictionary = key_code_dictionary

        self._simple_event_dictionary = {
            key_simple_event.get_key(): value_simple_event
            for key_simple_event, value_simple_event in self.simple_event_dictionary.items()
        }

    def get_transformed(self, simple_event_stream, u_input, input_device):
        for simple_event in simple_event_stream:
            _ = simple_event.get_key()
            next_simple_event = self._simple_event_dictionary.get(_, _None)
            if next_simple_event is not _None:
                if next_simple_event is not None:
                    _ = simple_event.time
                    yield next_simple_event.copy(time=_, held=simple_event.held)
                continue

            if self.key_code_dictionary is not None:
                key_code = self.key_code_dictionary.get(simple_event.key_code, _None)
                if key_code is not _None:
                    if key_code is not None:
                        _ = simple_event.time
                        yield simple_event.copy(
                            key_code=key_code, time=_, held=simple_event.held
                        )
                    continue

            yield simple_event
            continue


class RetimeRepeats(Transformation):
    """
    - injects key hold events with different timings
    """

    def __init__(self, initial_timeout=0.5, repeat_timeout=0.03):
        super().__init__()

        self.initial_timeout = initial_timeout
        self.repeat_timeout = repeat_timeout

    def get_transformed(self, event_stream, u_input, input_device):
        event_queue = queue.Queue(maxsize=1)

        def _consume_event_stream():
            for event in event_stream:
                if event.value == KEY_HOLD:
                    continue

                event_queue.put(event)
                event_queue.join()

        threading.Thread(target=_consume_event_stream, daemon=True).start()

        timeout = None
        while True:
            try:
                event = event_queue.get(timeout=timeout)

            except queue.Empty:
                hold_event = _get_event(event.code, KEY_HOLD)
                yield hold_event  # INFO delays repeats; usually ok

                timeout = self.repeat_timeout

            else:
                yield event  # INFO delays repeats; usually ok
                event_queue.task_done()

                timeout = self.initial_timeout if event.value == KEY_DOWN else None


class DelayEvents(Transformation):
    def __init__(self, seconds=0.001):
        super().__init__()

        self.seconds = seconds

    def get_transformed(self, event_stream, u_input, input_device):
        for event in event_stream:
            yield event
            time.sleep(self.seconds)


class StickModifiers(Transformation):
    def __init__(self, key_codes, trigger_seconds=0.33, stick_seconds=0.33):
        super().__init__()

        self.key_codes = key_codes
        self.trigger_seconds = trigger_seconds
        self.stick_seconds = stick_seconds

    def get_transformed(self, event_stream, u_input, input_device):
        trigger_times = {}
        stick_times = OrderedDict()

        for event in event_stream:
            if event.code in self.key_codes:
                if event.value == KEY_DOWN:  # start trigger
                    trigger_times[event.code] = time.monotonic()

                    if stick_times.pop(event.code, None) is None:  # still down
                        yield event

                elif event.value == KEY_UP:  # end trigger, start stick
                    trigger_time = trigger_times.get(event.code)
                    current_time = time.monotonic()
                    if (
                        trigger_time is not None
                        and (current_time - trigger_time) <= self.trigger_seconds
                    ):  # timeout trigger
                        del trigger_times[event.code]
                        stick_times[event.code] = current_time

                    else:
                        yield event
                        trigger_times.pop(event.code, None)

                else:
                    yield event

                continue

            trigger_times.clear()  # cancel trigger

            if len(stick_times) != 0:  # timeout stick
                current_time = time.monotonic()

                for key_code, stick_time in list(stick_times.items()):
                    if self.stick_seconds < (current_time - stick_time):
                        yield _get_event(key_code, KEY_UP)
                        del stick_times[key_code]

            yield event

            if len(stick_times) != 0:
                for key_code in reversed(stick_times.keys()):
                    yield _get_event(key_code, KEY_UP)
                stick_times.clear()


def _get_event(key_code, value, time_=None):
    if time_ is None:
        time_ = divmod(time.time_ns() // 1000, 1_000_000)
    sec, usec = time_
    return evdev.InputEvent(sec, usec, e_ecodes.EV_KEY, key_code, value)

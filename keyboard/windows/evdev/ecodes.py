from keyboard._ecodes import *
import pynput.keyboard._win32 as pk__win32
import pynput._util.win32 as p__win32
import pynput._util.win32_vks as p__win32_vks
import itertools


EV_KEY = 1


_KEYS = {p__win32_vks.PAUSE: KEY_PAUSE}


def _get_key(object):
    key_code = object.value if isinstance(object, pk__win32.Key) else object
    _ = key_code._parameters(True)
    scan_code = _["wScan"]
    if scan_code == 0:
        return _KEYS.get(key_code.vk)
    _ = (scan_code, (_["dwFlags"] & p__win32.KEYBDINPUT.EXTENDEDKEY) != 0)
    return _WIN32_SC_KEYS.get(_)


# ISO105/ANSI104
_E0_VK_NAMES = {
    "SNAPSHOT",
    "INSERT",
    "HOME",
    "PRIOR",
    "NUMLOCK",
    "DIVIDE",
    "DELETE",
    "END",
    "NEXT",
    "UP",
    "LWIN",
    "RMENU",
    "RWIN",
    "RCONTROL",
    "LEFT",
    "DOWN",
    "RIGHT",
}
_BOTH_VK_NAMES = {"NUMLOCK"}

_KEY_CODES = {}

_ = itertools.chain(
    (
        (object, name)
        for name, object in vars(p__win32_vks).items()
        if not name.startswith("_")
    ),
    zip(itertools.count(0x41), "ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
    zip(itertools.count(0x30), "0123456789"),
)
for virtual_key, name in sorted(_):
    _ = pk__win32.KeyCode
    key_code = (_._from_ext if name in _E0_VK_NAMES else _.from_vk)(virtual_key)

    key = _get_key(key_code)
    if key is None:
        continue

    _KEY_CODES.setdefault(key, key_code)

    if name in _BOTH_VK_NAMES:
        key_code = pk__win32.KeyCode.from_vk(virtual_key)
        _KEY_CODES.setdefault(_get_key(key_code), key_code)

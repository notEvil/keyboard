import pynput.keyboard as p_keyboard
import pynput._util as p__util
import evdev.ecodes as e_ecodes
import time


class KeyEvent:
    key_up = 0
    key_down = 1
    key_hold = 2


class InputEvent:
    def __init__(self, sec, usec, type, code, value):
        super().__init__()

        self.sec = sec
        self.usec = usec
        self.type = type
        self.code = code
        self.value = value


class InputDevice:
    def __init__(self, device_path):
        super().__init__()

        self.device_path = device_path

        self._events_object = _Events(
            win32_event_filter=self._filter_event,
            win32_get_suppress_after=self._get_suppress_after,
        )
        self._events = self._events_object.__enter__()

    def _filter_event(self, msg, data):
        if not self._get_suppress_after(msg, data):
            return False

    def _get_suppress_after(self, msg, data):
        return (data.flags & 0x00000010) == 0

    def read_loop(self):
        return self._events

    def grab(self):
        pass

    def ungrab(self):
        pass

    def __del__(self):
        self._events_object.__exit__(None, None, None)


class _Events(p__util.Events):
    _Listener = p_keyboard.Events._Listener

    def __init__(self, *args, **kwargs):
        self._functions = [self._on_press, self._on_release]

        _ = dict(on_press=self._on_press, on_release=self._on_release)
        super().__init__(*args, **(kwargs | _))

        self._previous_key = None

    def _event_mapper(self, object):  # workaround
        if object in self._functions:
            return super()._event_mapper(object)

        return object

    def _on_press(self, object):
        sec, usec = self._get_time()
        key = e_ecodes._get_key(object)
        _ = KeyEvent.key_hold if key == self._previous_key else KeyEvent.key_down
        input_event = InputEvent(sec, usec, e_ecodes.EV_KEY, key, _)
        self._previous_key = key
        return input_event

    def _on_release(self, object):
        self._previous_key = None
        sec, usec = self._get_time()
        _ = e_ecodes._get_key(object)
        return InputEvent(sec, usec, e_ecodes.EV_KEY, _, KeyEvent.key_up)

    def _get_time(self):
        seconds = time.time()
        sec = int(seconds)
        return (sec, int((seconds - sec) * 1e6))


class UInput:
    @staticmethod
    def from_device(device_path):
        return UInput()

    def __init__(self):
        super().__init__()

        self._controller = p_keyboard.Controller()

    def write_event(self, event):
        (
            self._controller.release
            if event.value == KeyEvent.key_up
            else self._controller.press
        )(e_ecodes._KEY_CODES[event.code])

    def syn(self):
        pass


_VALUE_STRINGS = {
    KeyEvent.key_up: "up",
    KeyEvent.key_down: "down",
    KeyEvent.key_hold: "hold",
}


def categorize(event):
    _ = "key event at {:0.6f}, {} ({}), {}"
    return _.format(
        event.sec + event.usec * 1e-6,
        event.code,
        e_ecodes.keys.get(event.code, "TODO"),
        _VALUE_STRINGS[event.value],
    )

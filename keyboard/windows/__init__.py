import keyboard
import time


class FixWindowsIn(keyboard.Transformation):
    """
    - Windows does not generate consistent events
      - e.g. LEFTCTRL up without previous down
    """

    def get_transformed(self, event_stream, u_input, input_device):
        key_codes = []

        for event in event_stream:
            if event.value == keyboard.KEY_DOWN:
                key_codes.append(event.code)

            else:
                if event.code not in key_codes:
                    continue

                if event.value == keyboard.KEY_UP:
                    key_codes.remove(event.code)

            yield event


class FixWindowsOut(keyboard.Transformation):
    """
    - Windows by default reacts to modifier key combinations
      - e.g. LEFTALT down + up, SHIFT down + CTRL down
      - keyboard.ToEvents unintentially triggers these combinations
    """

    def __init__(self, dead_key_code):
        super().__init__()

        self.dead_key_code = dead_key_code

    def get_transformed(self, event_stream, u_input, input_device):
        previous_event = None
        previous_time = None

        for event in event_stream:
            if event.code in keyboard.MODIFIER_BY_KEY_CODE:
                if event.value == keyboard.KEY_DOWN:
                    previous_time = time.monotonic()

                elif (
                    event.value == keyboard.KEY_UP
                    and event.code == previous_event.code
                    and previous_event.value == keyboard.KEY_DOWN
                    and (time.monotonic() - previous_time) <= 0.04
                ):
                    yield keyboard._get_event(
                        self.dead_key_code,
                        keyboard.KEY_DOWN,
                        time_=(event.sec, event.usec),
                    )
                    yield keyboard._get_event(
                        self.dead_key_code,
                        keyboard.KEY_UP,
                        time_=(event.sec, event.usec),
                    )

            yield event

            previous_event = event

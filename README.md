# Keyboard

*keyboard* is a Python package which provides transformations of keyboard events. It aims to be simple and extendible.

## Features

- Python
- low-level (works everywhere, does anything)
- simple configuration
- Windows support
    - [*evdev*](https://github.com/gvalkov/python-evdev) for Windows based on [*pynput*](https://github.com/moses-palmer/pynput)
- [*Master Password*](https://gitlab.com/MasterPassword/MasterPassword)

## Quickstart

for Windows see [windows](./windows)

- install Python
- install [*pipenv*](https://github.com/pypa/pipenv), e.g. with `python -m pip install pipenv`
- clone or download and extract this repository
- navigate to this repository
- execute `sudo python -m pipenv install`
- execute `sudo python -m pipenv run python "./example.py" --device "/path/to/your/keyboard/device"`

### kbdlayout.info.py

This script generates configuration parts for common use cases. It uses xml files from [http://kbdlayout.info/](http://kbdlayout.info/) to generate
- `(key, {modifier}): str`
    - useful to get the sequence of characters produced by the events
- `(key, {modifier}): (key, {modifier})` and `key: key`
    - useful to map a standard layout onto another, e.g. see [windows/example.py](./windows/example.py)

## If it doesn't work

.. don't despair!

If the example script doesn't work and you can't find out why, please create an issue and describe the problem. If its a problem I can fix, I'll try.

If the example script works, but your custom script doesn't, please strip your script of anything which isn't relevant to the problem. This trial and error usually exposes the problem. If you still can't fix the problem, let me know.

If your custom script works but doesn't behave as intended, use `keyboard.CategorizeEvents` and `keyboard.PrintSimpleEvents` for print debugging. Keep in mind that your operating system is still involved and might be the culprit (keyboard layout, shortcuts, ...).

## Master Password

If you don't know *Master Password*, check it out first; you can try it [here](https://js.masterpassword.app/). `keyboard.mpw.WithMasterpassword` replaces a specific input sequence with a generated password. It requires `characters={keyboard.SimpleEvent: str}` and a file containing a cryptographic hash of your master password. The input sequence is

`{master password}@{site}` optionally followed by `#{version}` and finalized by `KEY_TAB` or `KEY_ENTER`

On `@` the input is hashed. If the hash matches the master password hash, `{master password}@` is removed using backspace events.
On `KEY_TAB` or `KEY_ENTER` the input is removed again and split into site and version. The *Master Password* CLI is used to generate the password. Finally, the password is written using corresponding events.

- any simple event not in `characters` cancels the procedure
- `KEY_BACKSPACE`, by default, without modifiers removes the last character
- you can specify a version file which is used to remember versions

## Background

1. Context
    - keyboard events are a stream of key-down, key-hold and key-up events
    - events must yield a consistent state at any point in time
        - for each key, key-down --> [key-hold -->]* --> key-up
    - events from multiple keys interact, e.g. modifier keys
2. Issues
    - ensure consistency in case of multiple transformations
    - make it simple stupid
3. Solution
    - use a sequence of transformations instead of a tree or graph
    - require every transformation to yield a consistent stream of events
    - use intermediate representation of events which includes context, e.g. state of modifiers (class SimpleEvent)

## TODO

- find a unique name for this package